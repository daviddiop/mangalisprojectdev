({
    helperMethod : function() {

    }
});

({
    showToast: function(message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: type
        });
        toastEvent.fire();
    },
    sendHelper: function(component, getEmail, getSubject, getbody) {
        // call the server side controller method 
        console.log(component.get('v.recordId'));	
        var action = component.get("c.sendMailMethod");
        // set the 3 params to sendMailMethod method   
        action.setParams({
            'mMail': getEmail,
            'mSubject': getSubject,
            'mbody': getbody,
            'oppId': component.get('v.recordId'),
            'titleFile': component.get('v.titleFile')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true
                component.set("v.mailStatus", true);
                this.showToast('Email Sent successfully to '+ component.get('v.email'),'Success');
                $A.get("e.force:closeQuickAction").fire() 
            }
            else{
                this.showToast('Errore', 'Error');
            }
 
        });
        $A.enqueueAction(action);
    },
})