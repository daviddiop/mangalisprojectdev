({
    myAction : function(component, event, helper) {

    }
});

({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAttachment");
        action.setParams({
            'oppId': component.get('v.recordId'),
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                if (responseValue.error == false) {
                 var titleFile = responseValue.data;
                 component.set('v.titleFile',titleFile.Name);                  
                 component.set('v.showSpinner',true);                  
                } else {
                    helper.showToast(responseValue.message, 'Error');
                }
            } else {
                helper.showToast('Errore', 'Error');
            }
            component.set('v.showSpinner1', true);
        });
        $A.enqueueAction(action);
    },
    handleClose : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire() 
    },
    sendMail: function(component, event, helper) {
        console.log('ok');
        // when user click on Send button 
        // First we get all 3 fields values 	
        var getEmail = component.get("v.email");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        console.log("getSubject >>> "+getSubject);
        // check if Email field is Empty or not contains @ so display a alert message 
        // otherwise call call and pass the fields value to helper method    
        if ($A.util.isEmpty(getEmail) || !getEmail.includes("@")) {
            helper.showToast('Please Enter valid Email Address','Error');
        }
        else if ($A.util.isEmpty(getSubject)) {
                helper.showToast('Please Enter valid Subject ','Error');
        } 
        else if ($A.util.isEmpty(getbody)) {
            helper.showToast('The mail body is empty ','Error');
        } else {
            helper.sendHelper(component, getEmail, getSubject, getbody);
        }
    },
})