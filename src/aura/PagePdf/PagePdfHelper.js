({
    showToast: function(message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type: type
        });
        toastEvent.fire();
    }, helperSaveFile: function(component,sendEmail) {
        var action = component.get("c.savePDF");
        action.setParams({
            'recordId': component.get('v.recordId'),
             });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('ok');
                var responseValue = response.getReturnValue();
              
                    console.log(' not ok');
                    this.showToast('File was saved', 'Success');
                    $A.get('e.force:refreshView').fire();
                    if(sendEmail){
                        alert('send mail ok');
                        component.set('v.showGeneratePdfCmp',false);
                        component.set('v.showEmailSendCmp',true);
                    }else{
                        alert('send mail not ok');
                        $A.get("e.force:closeQuickAction").fire() 
                    }
                } else {
                    this.showToast(responseValue.message, 'Error');
                }
            
            component.set('v.showSpinner1', true);
        });
        $A.enqueueAction(action);
    },
})