({
    handleClose : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire() 
    },

        savePDFFromCont : function(cmp, event, helper) {
            console.log("ok");

            var action = cmp.get("c.savePDF");
            action.setParams({
                'recordId': cmp.get('v.recordId'),
                 });
                 console.log(cmp.get('v.recordId'));
            console.log('ok1');
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('ok2');
                if (state === "SUCCESS") {
                  //  alert('Attachment saved successfully');
                  
                  $A.get("e.force:closeQuickAction").fire();
                  helper.showToast('File was saved', 'Success');

                }
                else if (state === "INCOMPLETE") {
                    // do something
                    console.log('ok3');
                }
                    else if (state === "ERROR") {
                        console.log('ok4');
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(action);
        },
        handleSaveFileSendEmail : function(component, event, helper) {
        console.log("ok");
           helper.helperSaveFile(component,true);
        }
    
})