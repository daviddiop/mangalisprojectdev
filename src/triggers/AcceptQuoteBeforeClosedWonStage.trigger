trigger AcceptQuoteBeforeClosedWonStage on Opportunity (before Update) {
    List<Quote> quotes = [SELECT Id,Status,OpportunityId FROM Quote WHERE OpportunityId IN :Trigger.Old];
    Map<Id,Quote> OppQuoteMAp = new Map<Id,Quote>();
    for(Quote quote: quotes){
        OppQuoteMAp.put(quote.OpportunityId,quote);
    }
        for(Opportunity newOpp: Trigger.New) {
            if((OppQuoteMAp.get(newOpp.Id)==null || OppQuoteMAp.get(newOpp.Id).status!='Accepted') 
               && newOpp.StageName=='Fermée gagnante'){
                newOpp.addError('Cette étape nécessite un dévis accepté!');
            }
    }
}