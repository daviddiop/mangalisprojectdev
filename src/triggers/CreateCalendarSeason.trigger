trigger CreateCalendarSeason on period_season__c (after insert, after update) {
    if(trigger.isAfter){
        //recuperer le 1 janvier et le 31 decembre de l'année 
        Integer CurrentYear=Trigger.New[0].Start_Date__c.year();
        date firstDayCurrentYear= Date.newInstance(CurrentYear,1,1);
        date LastDayCurrentYear= Date.newInstance(CurrentYear,12,31);
        for(period_season__c ps : Trigger.New) {
            //Le trigger s'exécute sssi le fin de la période saison enregistré est le 31 decembre
            if(ps.End_Date__c==LastDayCurrentYear){
                        //recuperer les périodes saison de même année et de même hotel que l'enregistrement
                List<period_season__c> periodSeasons = [SELECT Id, Start_Date__c,End_Date__c,Hotel__c,Season__r.Name, Season__r.Id from
                                                        period_season__c where Hotel__c=:Trigger.New[0].Hotel__c ANd 
                                                        CurrentYear__c=:Trigger.New[0].CurrentYear__c ORDER BY Start_Date__c];
                //recuperer les periodes saisons qui ne sont pas des weekend et ceux des weekends exceptionnels
                List<period_season__c> periodSeasonHorsWeekEnds= new List<period_season__c>();
                for(period_season__c periode:periodSeasons){
                    Datetime sd=(Datetime)periode.Start_Date__c;
                    Datetime ed=(Datetime)periode.End_Date__c;
                    Date endd=periode.Start_Date__c.addDays(1);
                    if(sd.format('EEEE')!='Friday' || (sd.format('EEEE')=='Friday' && sd==firstDayCurrentYear) || (sd.format('EEEE')=='Friday' && periode.Season__r.Name!='Low Season') || (sd.format('EEEE')=='Saturday' && sd==firstDayCurrentYear) || periode.End_Date__c!=endd
                       || (ed.format('EEEE')=='Friday' && ed==LastDayCurrentYear) || (ed.format('EEEE')=='Saturday' && ed==LastDayCurrentYear)){
                           periodSeasonHorsWeekEnds.add(periode);
                       }
                }
                //Parcourir les periodes saisons recupérés et créer un nouveu calendarseason dés qu'il y'a nouveau saison
                date endDateNewCalendarSeason;
                date startDateNewCalendarSeason=periodSeasonHorsWeekEnds[0].Start_Date__c;
                string seasonNewCalendarSeason=periodSeasonHorsWeekEnds[0].Season__r.Id;
                List<Calendar_season__c> newCalendarSeasons= new  List<Calendar_season__c>();
                List<period_season__c> pseasons= new  List<period_season__c>();
                List<Calendar_season__c> Calendarseasons = [SELECT Id, Start_Date__c,End_Date__c,Hotel__c from Calendar_season__c];
                Map<Date,Calendar_season__c> MapCalendarseasons= new Map<Date,Calendar_season__c>();
                for(Calendar_season__c Calendarseason:Calendarseasons){
                    MapCalendarseasons.put(Calendarseason.Start_Date__c,Calendarseason);
                }
                for(Integer i=0; i<periodSeasonHorsWeekEnds.size();i++){
                    if(periodSeasonHorsWeekEnds[i].Season__r.Id!=seasonNewCalendarSeason && i!=periodSeasonHorsWeekEnds.size()){
                        endDateNewCalendarSeason = periodSeasonHorsWeekEnds[i-1].End_Date__c;
                        Date startDate = periodSeasonHorsWeekEnds[i].Start_Date__c;
                        String season = periodSeasonHorsWeekEnds[i].Season__r.Id;
                        Date startDateMoins2=periodSeasonHorsWeekEnds[i].Start_Date__c.addDays(-2);
                        Datetime stD=(Datetime)startDateMoins2;
                        if(periodSeasonHorsWeekEnds[i-1].Start_Date__c!=startDateMoins2 && stD.format('EEEE')=='Friday' && i!=0){
                            startDate=startDateMoins2;
                        }
                        Calendar_season__c newCalendarSeason= new  Calendar_season__c(Start_Date__c=startDateNewCalendarSeason,Current_Year__c=ps.CurrentYear__c,
                                                                                      Hotel__c=periodSeasonHorsWeekEnds[i].Hotel__c, Season__c=seasonNewCalendarSeason,End_Date__c=endDateNewCalendarSeason);
                        if(MapCalendarseasons.get(startDateNewCalendarSeason)!=null && MapCalendarseasons.get(startDateNewCalendarSeason).Hotel__c!=newCalendarSeason.Hotel__c){
                            newCalendarSeasons.add(newCalendarSeason);
                        }
                        if(MapCalendarseasons.get(startDateNewCalendarSeason)==null){
                            newCalendarSeasons.add(newCalendarSeason);
                            
                        }
                        startDateNewCalendarSeason=startDate;
                        seasonNewCalendarSeason=season;
                    }
                }
                Date startDateMoins2=startDateNewCalendarSeason.addDays(-2);
                Datetime stD=(Datetime)startDateMoins2;
                if(stD.format('EEEE')=='Friday'){
                    startDateNewCalendarSeason=startDateMoins2;
                }
                Calendar_season__c newCalendarSeason= new  Calendar_season__c(Start_Date__c=startDateNewCalendarSeason,
                                                                              Hotel__c=ps.Hotel__c, Season__c=ps.Season__c,End_Date__c=LastDayCurrentYear);
                if(MapCalendarseasons.get(startDateNewCalendarSeason)!=null && MapCalendarseasons.get(startDateNewCalendarSeason).Hotel__c!=newCalendarSeason.Hotel__c){
                    newCalendarSeasons.add(newCalendarSeason);
                }
                if(MapCalendarseasons.get(startDateNewCalendarSeason)==null){
                    newCalendarSeasons.add(newCalendarSeason);
                    
                }
                insert newCalendarSeasons;
                
            }
        }  
        
    }
}