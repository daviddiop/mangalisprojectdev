trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert, before update) {
	/********************************************************************************
    *
    *      Trigger on OpportunityLineItem to catch invalid sales price
    *
    ********************************************************************************/
    if(Trigger.isBefore){
        Map<Id, opportunityLineItem> idPbeOli = new Map<Id, opportunityLineItem>();
        for(OpportunityLineItem opportunityLineItem : Trigger.new) {
            idPbeOli.put(opportunityLineItem.PricebookEntryId, opportunityLineItem);
        }
        List<PricebookEntry> pricebookEntries = [SELECT Id, Tarif_Single__c, Tarif_Supplement_double__c, Tariff_min__c,
                                              Tariff_Max__c, Second_Offer__c, First_Offer__c FROM PricebookEntry WHERE Id IN :idPbeOli.keySet()];
        for (PricebookEntry pricebookEntry : pricebookEntries) {
          System.debug('### pricebookEntry : ' + pricebookEntry);
          for(OpportunityLineItem opportunityLineItem : Trigger.new) {
              if (opportunityLineItem.PricebookEntryId == pricebookEntry.Id){
                  System.debug('### Trigger.new : ' + Trigger.new);
                  if (pricebookEntry.Tarif_Single__c!=null) {
                     if (opportunityLineItem.UnitPrice!=pricebookEntry.Tarif_Single__c && opportunityLineItem.UnitPrice!=(pricebookEntry.Tarif_Supplement_double__c+pricebookEntry.Tarif_Single__c)) {
                    	System.debug('### Comparaison : ' + pricebookEntry.Tarif_Single__c);
                        System.debug('### Error : ' + opportunityLineItem.UnitPrice);
                        opportunityLineItem.UnitPrice.addError('The sales price must be equal to the tarif single ('+pricebookEntry.Tarif_Single__c+') or the tarif single plus the double supplement ('+ (pricebookEntry.Tarif_Supplement_double__c+pricebookEntry.Tarif_Single__c)+')');
                	}
                  } else if (pricebookEntry.Tariff_min__c!=null && pricebookEntry.Tariff_Max__c!=null) {
                      if (opportunityLineItem.UnitPrice < pricebookEntry.Tariff_min__c || opportunityLineItem.UnitPrice > pricebookEntry.Tariff_Max__c) {
                          opportunityLineItem.UnitPrice.addError('The sales price must be between the tarif min ('+pricebookEntry.Tariff_min__c+') and the tarif max ('+ pricebookEntry.Tariff_Max__c+')');
                      }
                  } else if (pricebookEntry.First_Offer__c!=null || pricebookEntry.Second_Offer__c!=null) {
                     System.debug('### Comparaison prices : '+ (Double)opportunityLineItem.UnitPrice+' And '+(Double)pricebookEntry.First_Offer__c);
                     if (opportunityLineItem.UnitPrice!=pricebookEntry.First_Offer__c && opportunityLineItem.UnitPrice!=pricebookEntry.Second_Offer__c) {
                        opportunityLineItem.UnitPrice.addError('The sales price must be equal to the first offer ('+pricebookEntry.First_Offer__c+') or the second offer ('+ pricebookEntry.Second_Offer__c +')');
                	 }
                  }
              }
          }
                
        }
    }
}