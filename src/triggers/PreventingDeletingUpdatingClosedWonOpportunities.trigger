//Trigger to prevent deleting or updating  opportunity closed won
trigger PreventingDeletingUpdatingClosedWonOpportunities on Opportunity (before delete, before update) {
    //Get User and him profilName
    Id idUser = userinfo.getUserId();
    User user = [SELECT Id, profile.name from User where Id=: idUser ];
    string profileName=user.profile.name;
    //browse new opportunities
    if(profileName != 'System Administrator' && trigger.isBefore){ 
        for(Opportunity newOpp: Trigger.New) {
            if(newOpp.StageName=='Fermée gagnante' && trigger.isDelete){ 
                newOpp.addError('Vous ne pouvez pas supprimer une opportunité fermée gagnée. Veuillez contacter l\'administrateur. Merci.');
            }
            if(trigger.isUpdate && Trigger.OldMap.get(newOpp.Id).contract.status!='Activated' &&
               Trigger.OldMap.get(newOpp.Id).StageName=='Fermée gagnante'){
                   newOpp.addError('Vous ne pouvez pas modifier une opportunité fermée gagnée. Veuillez contacter l\'administrateur. Merci.');
               }
        }
    }
}