trigger PeriodeSeasonDateVerification on period_season__c (before insert) {
    /********************************************************************************
    *
    *      Trigger on Period Season to catch duplicates based on Start Date and End Date
    *
    ********************************************************************************/
    if(Trigger.isBefore){
        List<Id> periodSeasonIds = new List<Id>();
        List<Id> seasonIds = new List<Id>();
        List<Id> hotelIds = new List<Id>();
        String query = 'SELECT Id, Season__c, Hotel__c, Start_Date__c, End_Date__c FROM period_season__c WHERE Hotel__c IN :hotelIds'+(Trigger.isInsert ? '':'Season__c  NOT IN :seasonIds');
        Map<Id, List<Date>> occupatedDates = new Map<Id, List<Date>>();
        for(period_season__c periodSeason : Trigger.new){
            seasonIds.add(periodSeason.Season__c);
            hotelIds.add(periodSeason.Hotel__c);
        }
        for(period_season__c periodSeason : Database.query(query)){
            if(!occupatedDates.containsKey(periodSeason.Hotel__c)){
                occupatedDates.put(periodSeason.Hotel__c, new List<Date>());
            }
            List<Date> dates = occupatedDates.get(periodSeason.Hotel__c);
            for(Date c = periodSeason.Start_Date__c; c <= periodSeason.End_Date__c; c = c.addDays(1)){
                dates.add(c);
            }
            occupatedDates.put(periodSeason.Hotel__c, dates);
            System.debug('Period Season: '+periodSeason);
        }
        System.debug(occupatedDates);
        for(period_season__c periodSeason : Trigger.new) {
            System.debug(periodSeason);
            System.debug(periodSeason.Start_Date__c);
            System.debug(periodSeason.End_Date__c);
            System.debug(occupatedDates.get(periodSeason.Hotel__c));
            if(occupatedDates.containsKey(periodSeason.Hotel__c)){
                List<String> dates = new List<String>();
                for(Date c = periodSeason.Start_Date__c; c <= periodSeason.End_Date__c; c = c.addDays(1)){
                    if(occupatedDates.get(periodSeason.Hotel__c).contains(c)){
                        dates.add(c.year()+'-'+c.month()+'-'+c.day());
                    }
                }
                if(dates.size() > 0)
                    periodSeason.addError('Il existe déjà une période de saison à ces dates : '+String.join( dates, ', ' ));
            }
        }
    }
}