<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Opportunity_expired_field</fullName>
        <field>Opportunity_expired__c</field>
        <literalValue>1</literalValue>
        <name>Check Opportunity expired field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity Contract is expired</fullName>
        <actions>
            <name>Check_Opportunity_expired_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Contract.EndDate  = TODAY()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
