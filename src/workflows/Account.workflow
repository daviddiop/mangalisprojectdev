<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alerter_SEEN</fullName>
        <ccEmails>marame.ndiaye@terangacloud.com</ccEmails>
        <description>Alerter SEEN</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Mangalis/Mangalis_1646660580828</template>
    </alerts>
    <alerts>
        <fullName>Alerter_manager_Noom_Hotel_Abidjan_Plateau_Compte_cr_ou_modifi</fullName>
        <description>Alerter manager Noom Hotel Abidjan Plateau Compte créé ou modifié</description>
        <protected>false</protected>
        <recipients>
            <recipient>salimata.ngom@terangacloud.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_manager</template>
    </alerts>
    <alerts>
        <fullName>Alerter_manager_Noom_Hotel_Conakry_Compte_cr_ou_modifi</fullName>
        <description>Alerter manager Noom Hotel Conakry Compte créé ou modifié</description>
        <protected>false</protected>
        <recipients>
            <recipient>salimata.ngom@terangacloud.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_manager</template>
    </alerts>
    <alerts>
        <fullName>Alerter_manager_Noom_Hotel_Niamey_Compte_cr_ou_modifi</fullName>
        <description>Alerter manager Noom Hotel Niamey Compte créé ou modifié</description>
        <protected>false</protected>
        <recipients>
            <recipient>salimata.ngom@terangacloud.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_manager</template>
    </alerts>
    <alerts>
        <fullName>Alerter_manager_Seen_Hotel_Abidjan_Plateau_Compte_cr_ou_modifi</fullName>
        <ccEmails>marame.ndiaye@terangacloud.com</ccEmails>
        <description>Alerter manager Seen Hotel Abidjan Plateau Compte créé ou modifié</description>
        <protected>false</protected>
        <recipients>
            <recipient>salimata.ngom@terangacloud.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_manager</template>
    </alerts>
    <alerts>
        <fullName>Alerter_manager_Yaas_Hotel_Almadie_Dakar_Compte_cr_ou_modifi</fullName>
        <description>Alerter manager Yaas Hotel Almadie Dakar Compte créé ou modifié</description>
        <protected>false</protected>
        <recipients>
            <recipient>salimata.ngom@terangacloud.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_manager</template>
    </alerts>
</Workflow>
