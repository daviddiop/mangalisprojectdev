<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alerter_le_manager_au_changement_de_proprietaire_de_piste</fullName>
        <ccEmails>marame.ndiaye@terangacloud.com</ccEmails>
        <description>Alerter le manager au changement de proprietaire de piste</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Owner_notification_email</template>
    </alerts>
    <alerts>
        <fullName>Lead_owner_change_notification</fullName>
        <ccEmails>marame.ndiaye@terangacloud.com</ccEmails>
        <description>Lead owner change notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Owner_notification_email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_RecordType_on_Change_Owner</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Yaas_Hotel_Almadies_Dakar</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecordType on Change Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>change_description_evenement</fullName>
        <field>Description_Evenement__c</field>
        <formula>&apos;NOW()&apos;</formula>
        <name>change description evenement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>dependance description evnt</fullName>
        <actions>
            <name>change_description_evenement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Organisation_de_r_unions_et_d_v_nements__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
