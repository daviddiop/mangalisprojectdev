import { LightningElement, api, track, wire } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import { refreshApex } from '@salesforce/apex';

import getPriceBookEntries from '@salesforce/apex/CreateOpportunityProducts.getPriceBookEntries';

const COLUMNCORPORTATE = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Tarif Single',
        fieldName: 'TarifSingle',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },
    {
        label: 'Supplement Double',
        fieldName: 'TarifSupplementDouble',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    }
];

const COLUMNINTERMEDIAIREAGV = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '1st offer LS',
        fieldName: 'FirstOfferLS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '2nd Offer LS',
        fieldName: 'SecondOfferLS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '1st offer MS',
        fieldName: 'FirstOfferMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },
    {
        label: '2nd Offer MS',
        fieldName: 'SecondOfferMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '1st offer HS',
        fieldName: 'FirstOfferHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },
    {
        label: '2nd Offer HS',
        fieldName: 'SecondOfferHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    }
];

const COLUMNINTERMEDIAIREWHOLS = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Whols Dynamic',
        fieldName: 'Whols',
        cellAttributes: { alignment: 'left' },
        type: 'text',
        sortable: "true"
    },
];

const COLUMNGROUPE = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Tariff Min LMS',
        fieldName: 'TariffMinLMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Max LMS',
        fieldName: 'TariffMaxLMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Min HS',
        fieldName: 'TariffMinHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Max HS',
        fieldName: 'TariffMaxHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },
];

export default class QuickOpportunityAction extends LightningElement {
    disabled = true;
    selectionPBE = true;
    title = 'Select Category Rooms';
    @api recordId;
    @track data;
    @track columns = COLUMNCORPORTATE;
    selectedData = [];
    allSelectedRows = [];
    segment = '';
    numberOfSelectedRows = 0;
    error;
    _wiredResult;
    @track selection = [];

    @track dataTable;
    @track pageSize = 5;
    @track isFirstPage = true;
    @track isLastPage = true;
    @track currentPageNumber = 1;
    @track totalPages = 0;

    // Get the data to display in the datatable via wired apex and initialize the columns according to the segment
    @wire(getPriceBookEntries, { recordId: '$recordId' })
    myPriceBookEntries(result) {
        this._wiredResult = result;
        if (result.data) {
            this.data = result.data.map(element => {
                if (element.Pricebook2.Segment__r?.Name == 'Corporates') {
                    this.columns = COLUMNCORPORTATE;
                    this.segment = 'Corporates';
                } else if (element.Pricebook2.Segment__r?.Name == 'Groupes') {
                    this.columns = COLUMNGROUPE;
                    this.segment = 'Groupes';
                } else if (element.Pricebook2.Segment__r?.Name == 'Intermediaires' && element.Hotel_Type_Client__r?.Rate_Code__c == 'WHOLS') {
                    this.columns = COLUMNINTERMEDIAIREWHOLS;
                    this.segment = 'IntermediairesWhols';
                } else {
                    this.columns = COLUMNINTERMEDIAIREAGV;
                    this.segment = 'IntermediairesAGV';
                }
                var testd = {};
                testd["Id"] = element.Id;
                testd["RoomType"] = element.Name;
                testd["Code"] = element.ProductCode;
                testd["ProductId"] = element.Product2Id;
                testd["Hotel"] = element.Product2.Hotel__c;
                testd["UnitPrice"] = element.UnitPrice;
                testd["Whols"] = element.Whols__c;
                testd["TarifSingle"] = element.Tarif_Single__c;
                testd["TarifSupplementDouble"] = element.Tarif_Supplement_double__c;
                testd["TariffMinLMS"] = element.Tariff_Min_Low_Medium_Season__c;
                testd["TariffMaxLMS"] = element.Tariff_Max_Low_Medium_Season__c;
                testd["TariffMinHS"] = element.Tariff_Min_High_Season__c;
                testd["TariffMaxHS"] = element.Tariff_Max_High_Season__c;
                testd["FirstOfferLS"] = element.First_Offer_Low_Season__c;
                testd["SecondOfferLS"] = element.Second_Offer_Low_Season__c;
                testd["FirstOfferMS"] = element.First_Offer_Medium_Season__c;
                testd["SecondOfferMS"] = element.Second_Offer_Medium_Season__c;
                testd["FirstOfferHS"] = element.First_Offer_High_Season__c;
                testd["SecondOfferHS"] = element.Second_Offer_High_Season__c;
                return testd;
            });
            this.preparePagination();
            this.error = undefined;
        } else {
            this.error = result.error;
            this.data = undefined;
        }
    }


    handleCancel(event) {
        refreshApex(this._wiredResult);
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    handleRowSelection(evt) {
        let rowSelected = evt.detail.selectedRows;
        let tab = Object.values(rowSelected);
        // List of selected items we maintain.
        let selectedItemsSet = new Set(this.allSelectedRows);
        // List of items currently loaded for the current view.
        let loadedItemsSet = new Set();
        this.dataTable.map((event) => {
            loadedItemsSet.add(event.Id);
        });
        if (tab.length > 0) {
            // List of selected items from the data table event.
            let updatedItemsSet = new Set();

            console.log('Selection : ' + JSON.stringify(this.selection));

            if (evt.detail.selectedRows) {
                evt.detail.selectedRows.map((event) => {
                    updatedItemsSet.add(event.Id);
                });

                // Add any new items to the selection list
                updatedItemsSet.forEach((Id) => {
                    if (!selectedItemsSet.has(Id)) {
                        selectedItemsSet.add(Id);
                    }
                });
                loadedItemsSet.forEach((Id) => {
                    if (selectedItemsSet.has(Id) && !updatedItemsSet.has(Id)) {
                        // Remove any items that were unselected.
                        selectedItemsSet.delete(Id);
                    }
                });

                console.log('selected item set : ' + JSON.stringify(selectedItemsSet));
            }

            this.disabled = false;
        } else {
            console.log('Selection : ' + JSON.stringify(this.selection));
            loadedItemsSet.forEach((Id) => {
                if (selectedItemsSet.has(Id)) {
                    // Remove any items that were unselected.
                    selectedItemsSet.delete(Id);
                }
            });
            this.disabled = true;
        }
        console.log('selected item set 2 : ' + JSON.stringify(selectedItemsSet));
        this.allSelectedRows = [...selectedItemsSet];
        console.log('selected item set 2 : ' + JSON.stringify(this.allSelectedRows));
        this.numberOfSelectedRows = this.allSelectedRows.length;
    }

    handleNext() {
        let selectedItemsSet = new Set(this.allSelectedRows);
        this.data.map((element) => {
            if (selectedItemsSet.has(element.Id)) {
                this.selectedData.push(element);
            }
        });
        this.selectionPBE = false;
        this.title = 'Edit Selected Category Rooms';
    }

    handlePrevious(event) {
        this.data = event.detail.data;
        this.segment = event.detail.segment;
        this.disabled = true;
        this.selectionPBE = true;
        this.numberOfSelectedRows = 0;
        this.allSelectedRows = [];
        this.selectedData = [];
        this.selection = [];
        this.title = 'Select Category Rooms';
    }

    preparePagination() {
        let countTotalPage = Math.ceil(this.data.length / this.pageSize);
        this.totalPages = countTotalPage > 0 ? countTotalPage : 1;
        this.currentPageNumber = 1;
        this.setPaginateData();

    }
    setPaginateData() {
        let data = [];
        let x = (this.currentPageNumber - 1) * this.pageSize;
        for (; x < (this.currentPageNumber) * this.pageSize; x++) {
            if (this.data[x]) {
                data.push(this.data[x]);
            }
        }
        let selectedItemsSet = new Set(this.allSelectedRows);
        let defaultSelection = [];
        console.log('All Selected rows : ' + JSON.stringify(this.allSelectedRows));
        data.map((element) => {
            if (selectedItemsSet.has(element.Id)) {
                defaultSelection.push(element.Id);
            }
        });
        console.log('All defaultSelection rows : ' + JSON.stringify(defaultSelection));
        this.selection = defaultSelection;
        this.dataTable = data;
        this.isLastPage = this.currentPageNumber == this.totalPages ? true : false;
        this.isFirstPage = this.currentPageNumber == 1 ? true : false;
    }

    onNext() {
        this.currentPageNumber = this.currentPageNumber + 1;
        this.setPaginateData();
    }

    onPrevious() {
        this.currentPageNumber = this.currentPageNumber - 1;
        this.setPaginateData();
    }

    onFirst() {
        this.currentPageNumber = 1;
        this.setPaginateData();
    }

    onLast() {
        this.currentPageNumber = this.totalPages;
        this.setPaginateData();
    }
}