import { LightningElement, wire, api, track } from 'lwc';
import getPriceBookEntries from '@salesforce/apex/grilleTarifiere.getPriceBookEntries';

const COLUMNCORPORATES = [

    {
        label: 'Category Room',
        fieldName: 'Room',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Type of Business',
        fieldName: 'Pricebook',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Rate Code',
        fieldName: 'RateCode',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Potential Nuity',
        fieldName: 'NuityPotential',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'

    },

    {
        label: 'Tariff Single',
        fieldName: 'TarifSingle',
        cellAttributes: { alignment: 'left' },
        type: 'currency',
        sortable: "true"
    },

    {
        label: 'Tariff Supplement Double',
        fieldName: 'TarifSupplementDouble',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },
]

const COLUMNGROUPES = [
    {
        label: 'Room Category',
        fieldName: 'Room',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Type of Business',
        fieldName: 'Pricebook',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Rate Code',
        fieldName: 'RateCode',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Potential Nuity',
        fieldName: 'NuityPotential',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'

    },

    {
        label: 'Tariff Min LMS',
        fieldName: 'TariffMinLMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Max LMS',
        fieldName: 'TariffMaxLMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Min HS',
        fieldName: 'TariffMinHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Tariff Max HS',
        fieldName: 'TariffMaxHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },
]

const COLUMNINTERMEDIAIRES = [
    {
        label: 'Room Category',
        fieldName: 'Room',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Type of Business',
        fieldName: 'Pricebook',
        type: 'text',
        wrapText: "true",
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Rate Code',
        fieldName: 'RateCode',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: '1st Offer LS',
        fieldName: 'FirstOfferLS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '2nd Offer LS',
        fieldName: 'SecondOfferLS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '1st Offer MS',
        fieldName: 'FirstOfferMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },
    {
        label: '2nd Offer MS',
        fieldName: 'SecondOfferMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: '1st Offer HS',
        fieldName: 'FirstOfferHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },
    {
        label: '2nd Offer HS',
        fieldName: 'SecondOfferHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    }
];

export default class PriceListByHotel extends LightningElement {
    @api recordId;
    @track columnGroup = COLUMNGROUPES;
    @track columnCorp = COLUMNCORPORATES;
    @track columnInter = COLUMNINTERMEDIAIRES;
    @track corpoData;
    @track groupData;
    @track interData;
    @track error;

    @track pageSize = 5;
    @track isFirstPage = true;
    @track isLastPage = true;
    @track currentPageNumber = 1;
    @track totalPages = 0;
    @track activeTab = 1;

    @track paginationList;
    @track recordList;

    @wire(getPriceBookEntries, { recordId: '$recordId' })
    myPricebookEntries({ data, error }) {
        if (data) {
            var mappedData = new Map();
            for (const key in data) {
                if (Object.hasOwnProperty.call(data, key)) {
                    const element = data[key].map(element => {
                        var testd = {};
                        testd["Room"] = element.Product2.Name;
                        testd["Pricebook"] = element.Pricebook2?.Name;
                        testd["RateCode"] = element.Hotel_Type_Client__r?.Rate_Code__c;
                        testd["NuityPotential"] = element.Hotel_Type_Client__r?.Nuity_Potential__c;
                        testd["TarifSingle"] = element?.Tarif_Single__c;
                        testd["TarifSupplementDouble"] = element?.Tarif_Supplement_double__c;
                        testd["TariffMinLMS"] = element?.Tariff_Min_Low_Medium_Season__c;
                        testd["TariffMaxLMS"] = element?.Tariff_Max_Low_Medium_Season__c;
                        testd["TariffMinHS"] = element?.Tariff_Min_High_Season__c;
                        testd["TariffMaxHS"] = element?.Tariff_Max_High_Season__c;
                        testd["FirstOfferLS"] = element?.First_Offer_Low_Season__c;
                        testd["SecondOfferLS"] = element?.Second_Offer_Low_Season__c;
                        testd["FirstOfferMS"] = element?.First_Offer_Medium_Season__c;
                        testd["SecondOfferMS"] = element?.Second_Offer_Medium_Season__c;
                        testd["FirstOfferHS"] = element?.First_Offer_High_Season__c;
                        testd["SecondOfferHS"] = element?.Second_Offer_High_Season__c;
                        return testd;
                    });
                    mappedData[key] = element;
                }
            }
            this.corpoData = mappedData['Corporates'];
            this.groupData = mappedData['Groupes'];
            this.interData = mappedData['Intermediaires'];
            this.recordList = this.corpoData;
            this.error = undefined;
            this.preparePagination();
        } else {
            this.error = error;
        }
    }

    preparePagination() {
        let countTotalPage = Math.ceil(this.recordList.length / this.pageSize);
        this.totalPages = countTotalPage > 0 ? countTotalPage : 1;
        this.currentPageNumber = 1;
        this.setPaginateData();

    }
    setPaginateData() {
        let data = [];
        let x = (this.currentPageNumber - 1) * this.pageSize;
        for (; x < (this.currentPageNumber) * this.pageSize; x++) {
            if (this.recordList[x]) {
                data.push(this.recordList[x]);
            }
        }
        this.paginationList = data;
        this.isLastPage = this.currentPageNumber == this.totalPages ? true : false;
        this.isFirstPage = this.currentPageNumber == 1 ? true : false;
    }

    handleNext() {
        this.currentPageNumber = this.currentPageNumber + 1;
        this.setPaginateData();
    }

    handlePrevious() {
        this.currentPageNumber = this.currentPageNumber - 1;
        this.setPaginateData();
    }

    onFirst() {
        this.currentPageNumber = 1;
        this.setPaginateData();
    }

    onLast() {
        this.currentPageNumber = this.totalPages;
        this.setPaginateData();
    }

    handleActive(event) {
        this.pageSize = 5;
        this.isFirstPage = true;
        this.isLastPage = true;
        this.currentPageNumber = 1;
        this.totalPages = 0;
        if (event.target.value == 1) {
            this.activeTab = 1;
            if (this.corpoData) {
                this.recordList = this.corpoData;
            }
        } else if (event.target.value == 2) {
            this.activeTab = 2;
            if (this.groupData) {
                this.recordList = this.groupData;
            }
        } else {
            this.activeTab = 3;
            if (this.interData) {
                this.recordList = this.interData;
            }
        }

        if (this.recordList) {
            this.preparePagination();
        }
    }

}