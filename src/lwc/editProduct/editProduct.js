import { api, LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';

import createOpportunityLineItems from '@salesforce/apex/CreateOpportunityProducts.createOpportunityLineItems';

const COLUMNCORPORTATE = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Supplement Double',
        fieldName: 'TarifSupplementDouble',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Unit Price',
        fieldName: 'UnitPrice',
        type: 'currency',
        editable: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    }
];

const COLUMNINTERMEDIAIREAGV = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Offer Low Season',
        fieldName: 'FirstOfferLS',
        type: 'currency',
        editable: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true',
    },

    {
        label: 'Offer Medium Season',
        fieldName: 'FirstOfferMS',
        type: 'currency',
        editable: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Offer High Season',
        fieldName: 'FirstOfferHS',
        type: 'currency',
        editable: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    }
];

const COLUMNINTERMEDIAIREWHOLS = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Whols Dynamic',
        fieldName: 'Whols',
        type: 'text',
        cellAttributes: { alignment: 'left' },
        sortable: "true"
    },

    {
        label: 'Unit Price',
        fieldName: 'UnitPrice',
        type: 'currency',
        sortable: 'true',
        cellAttributes: { alignment: 'left' },
        editable: 'true'
    },
];

const COLUMNGROUPE = [
    {
        label: 'Category Room',
        fieldName: 'RoomType',
        type: 'text',
        wrapText: 'true',
        cellAttributes: { alignment: 'left' },
        sortable: 'true'
    },

    {
        label: 'Tariff Low-Medium Season',
        fieldName: 'TariffMinLMS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true",
        editable: 'true'
    },

    {
        label: 'Tariff High Season',
        fieldName: 'TariffMinHS',
        type: 'currency',
        cellAttributes: { alignment: 'left' },
        sortable: "true",
        editable: 'true'
    },

];

export default class EditProduct extends LightningElement {
    @api tableData;
    @api segment;
    @track columns;
    @api previousData;
    @api opportunityId;
    @track errors;

    connectedCallback() {
        // Initialize the coloumns according to the segment
        if (this.segment == 'Corporates') {
            this.columns = COLUMNCORPORTATE;
        } else if (this.segment == 'Groupes') {
            this.columns = COLUMNGROUPE;
        } else if (this.segment == 'IntermediairesWhols') {
            this.columns = COLUMNINTERMEDIAIREWHOLS;
        } else {
            this.columns = COLUMNINTERMEDIAIREAGV;
        }
    }

    handleSave() {
        this.errors = [];
        var draftValues = this.template.querySelector("lightning-datatable").draftValues;
        var data = this.template.querySelector("lightning-datatable").data;
        var dataEdit = [];
        if (draftValues.length > 0) {
            var errors = [];
            for (let i = 0; i < data.length; i++) {
                var tb = {};
                // Verify the unit price enter by the user if the user make some changes, if the unit price doesn't valide display errors
                if (this.segment == 'Corporates') {
                    if (draftValues[i].UnitPrice == this.tableData[i].TarifSingle || draftValues[i].UnitPrice == (this.tableData[i].TarifSingle + this.tableData[i].TarifSupplementDouble)) {
                        tb = Object.assign({}, data[i], { 'UnitPrice': draftValues[i].UnitPrice ? draftValues[i].UnitPrice : data[i].UnitPrice });
                    } else errors.push({ 'id': i, 'message': 'Line ' + (i + 1) + ' : The unit price must be equal to the tarif single or the tarif single plus the double supplement.' });
                } else if (this.segment == 'Groupes') {
                    if ((draftValues[i].TariffMinLMS >= this.tableData[i].TariffMinLMS && draftValues[i].TariffMinLMS <= this.tableData[i].TariffMaxLMS) || (draftValues[i].TariffMinHS >= this.tableData[i].TariffMinHS && draftValues[i].TariffMinHS <= this.tableData[i].TariffMinHS)) {
                        tb = Object.assign({}, data[i], {
                            'TariffMinLMS': draftValues[i].TariffMinLMS ? draftValues[i].TariffMinLMS : data[i].TariffMinLMS, 'TariffMinHS': draftValues[i].TariffMinHS ? draftValues[i].TariffMinHS : data[i].TariffMinHS
                        });
                    } else errors.push({ 'id': i, 'message': 'Line ' + (i + 1) + ' : The LMS Price must be greater than or equal to the tariff min LMS or lower than or equal to the tariff max LMS and the HS Price must be greater than or equal to the tariff min HS or lower than or equal to the tariff max HS.' });
                } else if (this.segment == 'IntermediairesWhols') {
                    tb = Object.assign({}, data[i], { 'UnitPrice': draftValues[i].UnitPrice ? draftValues[i].UnitPrice : data[i].UnitPrice });
                } else {
                    if (draftValues[i].FirstOfferLS == this.tableData[i].FirstOfferLS || draftValues[i].FirstOfferLS == this.tableData[i].SecondOfferLS || draftValues[i].FirstOfferMS == this.tableData[i].FirstOfferMS || draftValues[i].FirstOfferMS == this.tableData[i].SecondOfferMS || draftValues[i].FirstOfferHS == this.tableData[i].FirstOfferHS || draftValues[i].FirstOfferHS == this.tableData[i].SecondOfferHS) {
                        tb = Object.assign({}, data[i], {
                            'FirstOfferLS': draftValues[i].FirstOfferLS ? draftValues[i].FirstOfferLS : data[i].FirstOfferLS, 'FirstOfferMS': draftValues[i].FirstOfferMS ? draftValues[i].FirstOfferMS : data[i].FirstOfferMS,
                            'FirstOfferHS': draftValues[i].FirstOfferHS ? draftValues[i].FirstOfferHS : data[i].FirstOfferHS
                        });
                    } else errors.push({ 'id': i, 'message': 'Line ' + (i + 1) + ' : The LS Price must be equal to the First Offer LS or the Second Offer LS, The MS Price must be equal to the First Offer MS or the Second Offer MS, and The HS Price must be equal to the First Offer HS or the Second Offer HS.' });
                }
                dataEdit.push(tb);
            }
            if (errors.length > 0) {
                this.errors = errors;
                dataEdit = [];
                draftValues = [];
            }
        } else dataEdit = data;

        // Prepare the data to create
        if (dataEdit.length > 0 && this.errors?.length <= 0) {
            var oppProductList = [];
            dataEdit.forEach(element => {
                oppProductList.push({
                    OpportunityId: this.opportunityId,
                    Product2Id: element.ProductId,
                    Hotel__c: element.hotel,
                    UnitPrice: element.UnitPrice,
                    Whols__c: element?.Whols,
                    Price_LS__c: element?.FirstOfferLS,
                    Price_MS__c: element?.FirstOfferMS,
                    Price_HS__c: this.segment == 'Groupes' ? element?.TariffMinHS : element?.FirstOfferHS,
                    Price_LMS__c: element?.TariffMinLMS,
                    Tarif_Supplement_Double__c: element?.TarifSupplementDouble,
                    Quantity: 1
                });
            });

            // Create the data via apex
            createOpportunityLineItems({ oppProducts: JSON.stringify(oppProductList) })
                .then((result) => {
                    if (result === true) {
                        this.showToast('Success!!', 'The opportunity products have been successfully created', 'success');
                        // Notify LDS that you've changed the record outside its mechanisms.
                        getRecordNotifyChange([{ recordId: this.opportunityId }]);
                    } else {
                        this.showToast('Error!!', 'something went wrong please try again', 'error');
                    }
                })
                .catch((error) => {
                    this.showToast('Error!!', 'something went wrong please try again', 'error');
                });
        }
    }

    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant
            })

        );
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    handlePrevious() {
        this.dispatchEvent(new CustomEvent('previous', {
            detail: { 'data': this.previousData, 'segment': this.segment }
        }));
    }

    handleCancel(event) {
        this.dispatchEvent(new CustomEvent('cancel'));
    }

}