import { LightningElement, api } from 'lwc';
import PRICE_NAME_FIELD from '@salesforce/schema/PricebookEntry.Pricebook2Id';
import PRODUCT_NAME_FIELD from '@salesforce/schema/PricebookEntry.Product2Id';
import NAME_HTC_FIELD from '@salesforce/schema/PricebookEntry.Hotel_Type_Client__c';
import NAME_SEASON_FIELD from '@salesforce/schema/PricebookEntry.SeasonHTC__c';
import TARIF_MIN_LMS_FIELD from '@salesforce/schema/PricebookEntry.Tariff_Min_Low_Medium_Season__c';
import TARIF_MAX_LMS_FIELD from '@salesforce/schema/PricebookEntry.Tariff_Max_Low_Medium_Season__c';
import TARIF_MIN_HS_FIELD from '@salesforce/schema/PricebookEntry.Tariff_Min_High_Season__c';
import TARIF_MAX_HS_FIELD from '@salesforce/schema/PricebookEntry.Tariff_Max_High_Season__c';
import TARIF_CURRENCY_FIELD from '@salesforce/schema/PricebookEntry.CurrencyIsoCode';
import ACTIVE_FIELD from '@salesforce/schema/PricebookEntry.IsActive';
import CREATE_BY_FIELD from '@salesforce/schema/PricebookEntry.CreatedById';
import MODIFIEBY_FIELD from '@salesforce/schema/PricebookEntry.LastModifiedById';

export default class PbeTarifMinMax extends LightningElement {
    @api recordId;
    @api objectApiName;

    fields = [PRICE_NAME_FIELD, PRODUCT_NAME_FIELD, NAME_HTC_FIELD, NAME_SEASON_FIELD, TARIF_CURRENCY_FIELD, TARIF_MIN_LMS_FIELD, TARIF_MAX_LMS_FIELD, TARIF_MIN_HS_FIELD, TARIF_MAX_HS_FIELD, ACTIVE_FIELD, CREATE_BY_FIELD, MODIFIEBY_FIELD];

    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
        this.template.querySelector('lightning-record-form').submit(fields);
    }
}