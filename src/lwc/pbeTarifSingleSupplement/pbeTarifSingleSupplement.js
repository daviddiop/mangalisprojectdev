import { LightningElement, api } from 'lwc';
import PRICE_NAME_FIELD from '@salesforce/schema/PricebookEntry.Pricebook2Id';
import PRODUCT_NAME_FIELD from '@salesforce/schema/PricebookEntry.Product2Id';
import LIST_PRICE_FIELD from '@salesforce/schema/PricebookEntry.UnitPrice';
import NAME_HTC_FIELD from '@salesforce/schema/PricebookEntry.Hotel_Type_Client__c';
import TARIF_SINGLE_FIELD from '@salesforce/schema/PricebookEntry.Tarif_Single__c';
import TARIF_SUPPLEMENT_FIELD from '@salesforce/schema/PricebookEntry.Tarif_Supplement_double__c';
import TARIF_CURRENCY_FIELD from '@salesforce/schema/PricebookEntry.CurrencyIsoCode';
import ACTIVE_FIELD from '@salesforce/schema/PricebookEntry.IsActive';
import CREATE_BY_FIELD from '@salesforce/schema/PricebookEntry.CreatedById';
import MODIFIEBY_FIELD from '@salesforce/schema/PricebookEntry.LastModifiedById';

export default class PbeTarifSingleSupplement extends LightningElement {
    @api recordId;
    @api objectApiName;

    fields = [PRICE_NAME_FIELD,PRODUCT_NAME_FIELD,NAME_HTC_FIELD, LIST_PRICE_FIELD, TARIF_SINGLE_FIELD, TARIF_SUPPLEMENT_FIELD, TARIF_CURRENCY_FIELD,ACTIVE_FIELD,CREATE_BY_FIELD,MODIFIEBY_FIELD];

    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
        this.template.querySelector('lightning-record-form').submit(fields);
    }
}