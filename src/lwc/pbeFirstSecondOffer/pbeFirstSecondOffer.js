import { LightningElement, api } from 'lwc';
import PRICE_NAME_FIELD from '@salesforce/schema/PricebookEntry.Pricebook2Id';
import PRODUCT_NAME_FIELD from '@salesforce/schema/PricebookEntry.Product2Id';
import NAME_HTC_FIELD from '@salesforce/schema/PricebookEntry.Hotel_Type_Client__c';
import NAME_SEASON_FIELD from '@salesforce/schema/PricebookEntry.SeasonHTC__c';
import FIRST_OFFER_LS_FIELD from '@salesforce/schema/PricebookEntry.First_Offer_Low_Season__c';
import SECOND_OFFER_LS_FIELD from '@salesforce/schema/PricebookEntry.Second_Offer_Low_Season__c';
import FIRST_OFFER_MS_FIELD from '@salesforce/schema/PricebookEntry.First_Offer_Medium_Season__c';
import SECOND_OFFER_MS_FIELD from '@salesforce/schema/PricebookEntry.Second_Offer_Medium_Season__c';
import FIRST_OFFER_HS_FIELD from '@salesforce/schema/PricebookEntry.First_Offer_High_Season__c';
import SECOND_OFFER_HS_FIELD from '@salesforce/schema/PricebookEntry.Second_Offer_High_Season__c';
import TARIF_CURRENCY_FIELD from '@salesforce/schema/PricebookEntry.CurrencyIsoCode';
import ACTIVE_FIELD from '@salesforce/schema/PricebookEntry.IsActive';
import CREATE_BY_FIELD from '@salesforce/schema/PricebookEntry.CreatedById';
import MODIFIEBY_FIELD from '@salesforce/schema/PricebookEntry.LastModifiedById';
export default class PbeFirstSecondOffer extends LightningElement {
    @api recordId;
    @api objectApiName;

    fields = [PRODUCT_NAME_FIELD, PRICE_NAME_FIELD, NAME_HTC_FIELD, NAME_SEASON_FIELD, FIRST_OFFER_LS_FIELD, SECOND_OFFER_LS_FIELD, FIRST_OFFER_MS_FIELD, SECOND_OFFER_MS_FIELD, FIRST_OFFER_HS_FIELD, SECOND_OFFER_HS_FIELD, TARIF_CURRENCY_FIELD, ACTIVE_FIELD, CREATE_BY_FIELD, MODIFIEBY_FIELD];
    handleSubmit(event) {
        event.preventDefault(); // stop the form from submitting
        const fields = event.detail.fields;
        this.template.querySelector('lightning-record-form').submit(fields);
    }
}