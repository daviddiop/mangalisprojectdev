@isTest
public class calendarSeasonTest {
    @testSetup static void setup(){
        Season__c season = new Season__c(Name='Season Test');
        insert season;
        Hotel__c hotel = new Hotel__c(Name='Hotel periode Test');
        insert hotel;
        period_season__c periode = new period_season__c(Season__c=season.Id, Hotel__c=hotel.Id, Start_Date__c=Date.newInstance(2023, 1, 1), End_Date__c=Date.newInstance(2023, 1, 31));
        insert periode;
        Event ev = new Event(Subject = 'Test event', StartDateTime = DateTime.newInstance(2023, 1, 1, 10, 0, 0), EndDateTime = DateTime.newInstance(2023, 1, 1, 10, 0, 0));
        insert ev;
    }
    
    @isTest static void listPeriodeseason(){
        Hotel__c hotel = [SELECT Id, Name FROM Hotel__c LIMIT 1];
        System.debug('### hotel : '+ hotel);
        // Perform test
        Test.startTest();
        List<period_season__c> periods = calendarSeason.listPeriodeseason(hotel.Id);
        System.assertEquals(1, periods.size(), 'No expected result returned');
        Test.stopTest();
    }
    
    @isTest static void fetchEvents(){
        Test.startTest();  
        List<Event> events = calendarSeason.fetchEvents(); 
        System.assertEquals(1, events.size(), 'No expected result returned');
        Test.stopTest();
    }
}