public with sharing class CreateOpportunityProducts {
    @AuraEnabled(cacheable=true)
    public static List<PricebookEntry> getPriceBookEntries (Id recordId){
        List<PricebookEntry> namelist = new List<PricebookEntry>();
        try {
            // Get the pricebook related to this opportunity
            Opportunity opportunity = [SELECT Id, Pricebook2Id, Hotels__c FROM Opportunity WHERE Id=:recordId WITH SECURITY_ENFORCED];
            // Get the all the productId from all the  opportunity products related to this opportunity
            List<OpportunityLineItem> opportunityLineItems = [SELECT Product2Id FROM OpportunityLineItem WHERE OpportunityId=:recordId WITH SECURITY_ENFORCED];
            List<Id> productIds = new List<Id>();
            for (OpportunityLineItem opt : opportunityLineItems) {
                productIds.add(opt.Product2Id);
            }
            // Get all the pricebook entries which aren't added to this opportunity
            namelist = [SELECT Id, Product2Id, Product2.Hotel__c, Pricebook2.Segment__r.Name, Name, ProductCode, UnitPrice, Hotel_Type_Client__r.Rate_Code__c, Tarif_Single__c, Tarif_Supplement_double__c, 
            Tariff_Min_Low_Medium_Season__c, Tariff_Max_Low_Medium_Season__c, Tariff_Min_High_Season__c, Tariff_Max_High_Season__c, First_Offer_Low_Season__c, Second_Offer_Low_Season__c, First_Offer_Medium_Season__c, Second_Offer_Medium_Season__c,
            First_Offer_High_Season__c, Second_Offer_High_Season__c, Whols__c FROM PricebookEntry WHERE Pricebook2Id=:opportunity.Pricebook2Id AND Product2Id NOT IN :productIds WITH SECURITY_ENFORCED ];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return namelist;
    }

    @AuraEnabled
   public static Boolean createOpportunityLineItems(String oppProducts){
      List<OpportunityLineItem> oppProductList=(List<OpportunityLineItem>) System.JSON.deserialize(oppProducts, List<OpportunityLineItem>.Class);
      if(!oppProductList.isEmpty() && OpportunityLineItem.sObjectType.getDescribe().isCreateable()==true){
          insert oppProductList;
          return true;
       }
       return false;
   }
}