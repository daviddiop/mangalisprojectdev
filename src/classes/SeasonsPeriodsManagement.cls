public class SeasonsPeriodsManagement {
    @AuraEnabled
    public static void createWeekendsDatasForHotel(Id recordId){
        List<period_season__c> periodesToInsert = new List<period_season__c>();
        Id lowSeasonId = [SELECT Id FROM Season__c WHERE Name='Low Season'].Id;
        for(Date current = Date.newInstance((Date.today()).year(), 1, 1); current <= Date.newInstance((Date.today()).year(), 12, 31); current = current.addDays(1)){
            if(current.month() != 5 && current.month() != 6){
                DateTime myDateTime = (DateTime) current;
                String dayOfWeek = myDateTime.format('E');
                if(dayOfWeek == 'Fri'){
                    periodesToInsert.add(new period_season__c(
                        Hotel__c = recordId,
                        Season__c = lowSeasonId,
                        Start_Date__c = current,
                        End_Date__c =current.addDays(1)
                    ));
                }
            }
        }
        System.debug(periodesToInsert);
        insert periodesToInsert;
    }
    /**
    public static void createWeekendsDatas(){
        List<period_season__c> periodesToInsert = new List<period_season__c>();
        Id mediumSeasonId = [SELECT Id FROM Season__c WHERE Name='Medium Season'].Id;
        List<Hotel__c> hotels = [SELECT Id, Name FROM Hotel__c];
        for(Date current = Date.newInstance((Date.today()).year(), 1, 1); current <= Date.newInstance((Date.today()).year(), 10, 31); current = current.addDays(1)){
            DateTime myDateTime = (DateTime) current;
            String dayOfWeek = myDateTime.format('E');
            if(dayOfWeek == 'Fri'){
                for(Hotel__c hotel : hotels){
                    periodesToInsert.add(new period_season__c(
                      Hotel__c = hotel.Id,
                      Season__c = mediumSeasonId,
                      Start_Date__c = current,
                      End_Date__c =current.addDays(1)
                    ));
                }
            }
        }
        System.debug(periodesToInsert);
        insert periodesToInsert;
    }
 */
@AuraEnabled(cacheable=true)
    public static boolean checkIfAnnualWeekendSeasonPeriodsCreated(Id recordId){
        for(List<period_season__c> periods : [SELECT Start_Date__c, End_Date__c FROM period_season__c WHERE Hotel__c = :recordId]){
            for(period_season__c period : periods){
                DateTime myDateTime = (DateTime) period.Start_Date__c;
                String dayOfWeek = myDateTime.format('E');
                if(dayOfWeek == 'Fri'){
                    return true;
                }
            }
        }

        return false;
    }
}