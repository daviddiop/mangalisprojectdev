@isTest
public class TestOpportunityLineItemTrigger {
    @testSetup static void setup() {
		Account acc = new Account(Name = 'Test Account', Phone='+221777777777');
        insert acc;
      
        //get standard pricebookId
        Id  standardPbId = Test.getStandardPricebookId();
        List<Pricebook2> pbks = new List<Pricebook2>();
        for (Integer i=0;i<3;i++) {
            pbks.add(new Pricebook2 (Name='Test Pricebook '+i, isActive=true));
        }
        insert pbks;
        
        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
        insert prd1;
        
        // Insert standard price book entries for this product
        PricebookEntry stanPbes = new PricebookEntry(Product2ID=prd1.Id,Pricebook2ID=standardPbId, UnitPrice=50000, isActive=true);
        insert stanPbes;
        
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        for (Integer i=0; i<3; i++) {
            if (i==0){
                pbes.add(new PricebookEntry (Product2ID=prd1.Id,Pricebook2ID=pbks[i].Id,Tarif_Single__c=50000,Tarif_Supplement_double__c=14000, UnitPrice=50000, isActive=true));
            } else if (i==1){
                pbes.add(new PricebookEntry (Product2ID=prd1.Id,Pricebook2ID=pbks[i].Id,Tariff_min__c=80000,Tariff_Max__c=100000, UnitPrice=80000, isActive=true));
            } else {
                pbes.add(new PricebookEntry (Product2ID=prd1.Id,Pricebook2ID=pbks[i].Id,First_Offer__c=90000,Second_Offer__c=110000, UnitPrice=90000, isActive=true));
            }
        }        
        insert pbes;
        
        Opportunity opp1 = new Opportunity (Name='Opp1 Test',StageName='Prospection',CloseDate=Date.today(),Pricebook2Id = standardPbId,Type_Compte__c='Direct', AccountId = acc.Id);
        insert opp1;
	}
    
    @isTest static void testTriggerTarifSingleSupplement(){
        Product2 product = [SELECT Id FROM Product2 WHERE Name='Test Product Entry 1' LIMIT 1];
        Opportunity opp1 = [SELECT Id FROM Opportunity WHERE Name='Opp1 Test' LIMIT 1];
        PricebookEntry pbe1 = [SELECT Id FROM PricebookEntry WHERE (Product2Id=:product.Id AND Tarif_Single__c=50000 AND Tarif_Supplement_double__c=14000)];
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id, UnitPrice=1000);
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(lineItem1, false);
        Test.stopTest();
        // Verify
        if (!result.isSuccess()) {
            System.assert(!result.isSuccess());
        	System.assertNotEquals(null, result.getErrors().size());
    	}
    
    }
    
    @isTest static void testTriggerFirstSecondOffer(){
        Product2 product = [SELECT Id FROM Product2 WHERE Name='Test Product Entry 1' LIMIT 1];
        Opportunity opp1 = [SELECT Id FROM Opportunity WHERE Name='Opp1 Test' LIMIT 1];
        PricebookEntry pbe1 = [SELECT Id FROM PricebookEntry WHERE (Product2Id=:product.Id AND First_Offer__c=90000 AND Second_Offer__c=110000)];
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id, UnitPrice=1000);
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(lineItem1, false);
        Test.stopTest();
        // Verify
        if (!result.isSuccess()) {
            System.assert(!result.isSuccess());
        	System.assertNotEquals(null, result.getErrors().size());
    	}
    
    }
    
    @isTest static void testTriggerTarifMinMax(){
        Product2 product = [SELECT Id FROM Product2 WHERE Name='Test Product Entry 1' LIMIT 1];
        Opportunity opp1 = [SELECT Id FROM Opportunity WHERE Name='Opp1 Test' LIMIT 1];
        PricebookEntry pbe1 = [SELECT Id FROM PricebookEntry WHERE (Product2Id=:product.Id AND Tariff_min__c=80000 AND Tariff_Max__c=100000)];
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id, UnitPrice=1000);
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(lineItem1, false);
        Test.stopTest();
        // Verify
        if (!result.isSuccess()) {
            System.assert(!result.isSuccess());
        	System.assertNotEquals(null, result.getErrors().size());
    	}
    
    }
}