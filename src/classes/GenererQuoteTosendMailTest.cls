@isTest
public with sharing class GenererQuoteTosendMailTest {
    @testSetup
    static void setup(){
        Account acc = new Account(Name='Test', Email__c='test@test.com');
        insert acc;
        Opportunity testOpportunity = new Opportunity(Name='test',Type_Compte__c='Direct',StageName='Close Won',CloseDate=System.today(), AccountId=acc.Id);
        insert testOpportunity;
        Quote quote = new Quote(
            OpportunityId = testOpportunity.Id,
            Name ='Ter',
            Created_Date__c= System.today(),
            ExpirationDate= System.today()+30,
            Status= 'Accepted'
        );
        insert quote;
    }
    
    @isTest
    static void savePDFTest(){
        Quote quote = [SELECT Id FROM Quote WHERE Name='Ter' LIMIT 1];
        Test.startTest();
        GenererQuoteToSendMail.savePDF(quote.Id);
        Test.stopTest();
    }
    
    
    @isTest
    static void testSavePdf2() {
        Test.startTest();
        GenererQuoteToSendMail.savePdf(null);
        Test.stopTest();
    }
    @isTest
    static void testSendMail() {
        Quote quote = [SELECT Id FROM Quote WHERE Name='Ter' LIMIT 1];
        Test.startTest();
        GenererQuoteToSendMail.sendMailMethod('test@gmail.com','test','tests',quote.Id,'Test');
        Test.stopTest();
    } 
    @isTest
    static void testGetAttachment() {
        Quote quote = [SELECT Id FROM Quote WHERE Name='Ter' LIMIT 1];
        Test.startTest();
        GenererQuoteToSendMail.getAttachment(quote.Id);
        Test.stopTest();
    } 
}