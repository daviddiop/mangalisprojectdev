@isTest
public class GenererQuotepdfTest {
    @TestSetup
    static void setup(){
        Account acc = new Account(Name='test');
        insert acc;
        Segment__c segment = new Segment__c(Name='Segment Test');
        insert segment;
        Hotel_Type_Client__c htc = new Hotel_Type_Client__c(Name='HTC Test', Rate_Code__c='PCR1');
        insert htc;
        Pricebook2 pricebook = new Pricebook2(Name='Pricebook Test',Segment__c=segment.Id, Hotel_Type_Client__c= htc.Id);
        insert pricebook;
        Opportunity opps = new Opportunity(Name='test',Type_Compte__c='Direct',StageName='Close Won',CloseDate=System.today(), Pricebook2Id=pricebook.Id);
        insert opps;
        Quote quote = new Quote(
            //AccountId = acc.Id,
            OpportunityId = opps.Id,
            Name ='Ter',
            Created_Date__c= System.today(),
            Status= 'Accepted'
        );
        insert quote;
        List<Product2> produits = new List<Product2>();
        produits.add(new Product2(Name='test product'));
        produits.add(new Product2(Name='productTest',CurrencyIsoCode='XOF'));
        insert produits;
        List<PriceBookEntry> standPbks = new List<PriceBookEntry>();
        for( Product2 prod : produits){
            standPbks.add(new PriceBookEntry(
            Product2Id=prod.Id,
            Pricebook2Id=Test.getStandardPricebookId(),
            UnitPrice=2000,
            Tarif_Single__c=2000,
            IsActive=true
        	));
        }
        insert standPbks;
        List<PriceBookEntry> pbks = new List<PriceBookEntry>();
        for( Product2 prod : produits){
            pbks.add(new PriceBookEntry(
            Product2Id=prod.Id,
            Pricebook2Id=pricebook.Id,
            UnitPrice=2000,
            Tarif_Single__c=2000,
            IsActive=true
        	));
        }
        insert pbks;
        List<QuoteLineItem> quoteLines = new List<QuoteLineItem>();
        for(PricebookEntry pbk : pbks) {
            quoteLines.add(new QuoteLineItem(
            QuoteId=quote.Id,
            Product2Id=pbk.Product2Id,
            UnitPrice=114000,
            PricebookEntryId=pbk.Id,
            Quantity=2
        ));
        }
        insert quoteLines;
    }

    @isTest
    static void accList(){
        Quote quote = [SELECT Id FROM Quote WHERE Name='Ter' LIMIT 1];
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(quote);
            PageReference pageRef = Page.GenererPdf;
            Test.setCurrentPage(pageRef);
            new GenererQuotepdf(sc);
        Test.stopTest();
    }

}