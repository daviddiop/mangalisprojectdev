/**
 * @description       : 
 * @author            : Fama Ndiouck
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : Fama Ndiouck
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   04-27-2022   Fama Ndiouck   Initial Version
**/
public with sharing class GenererQuoteToSendMail {
    public GenererQuoteToSendMail() {

    }
    

@auraEnabled
public static void savePDF(Id recordId){
    Map<String,Object> response = new Map<String,Object>();
    String urlPdf = 'PdfIndNoAssenteismo';
    try {
        response.put('error',false);
        PageReference pdfPage = new PageReference('/apex/GenererPdf?id='+recordId);
        //pdfPage.getParameters().put('Id', oppId); 
        Quote quote = [SELECT Account.Name, ExpirationDate FROM Quote WHERE Id = :recordId WITH SECURITY_ENFORCED];
        System.debug('### Quote : '+ quote);
        Blob pdfContent;
        if(Test.isRunningTest()) {
            pdfContent = Blob.valueOf('Unit.Test');
        } else {
            pdfContent = pdfPage.getContentAsPDF();
        }
        ContentVersion contentVersion1 = new ContentVersion(
            Title='Devis '+quote.Account.Name+' '+(quote.ExpirationDate).year()+'.PDF',
            PathOnClient ='/.pdf',
            VersionData = pdfContent,
            origin = 'H'
        );
        insert contentVersion1;
        ContentVersion contentVersion2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion1.Id WITH SECURITY_ENFORCED];
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = recordId;
        contentlink.contentdocumentid = contentVersion2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;
        Attachment attach1= new Attachment();
        attach1.ParentId = recordId;
        attach1.Name = 'Devis '+quote.Account.Name+' '+(quote.ExpirationDate).year()+'.PDF';
        attach1.Body = pdfContent;
        attach1.contentType = 'application/pdf';
        attach1.IsPrivate = false;
        insert attach1;
        System.debug('attach1 >>> '+attach1);
    } catch (Exception e) {
        System.debug('messages >>> '+e.getMessage());
        response.put('error',true);
        response.put('message',e.getMessage());
        response.put('cause',e.getCause());
        System.debug('cause >>> '+e.getCause());
    }
}
@AuraEnabled
public static void sendMailMethod(String mMail ,String mSubject ,String mbody, String recordId,String titleFile){
System.debug('mail >>'+mMail );
System.debug('recordId >>'+recordId );
    List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
    // Step 1: Create a new Email
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   // Step 2: Set list of people who should get the email
      List<String> sendTo = new List<String>();
      sendTo.add(mMail);
      mail.setToAddresses(sendTo);
   // Step 3: Set who the email is sent from
      mail.setReplyTo('noreply@gmail.com'); // change it with your mail address.
      mail.setSenderDisplayName(getQuoteInfo(recordId).Owner.Name);
   // Step 4. Set email contents - you can use variables!
     mail.setSubject(mSubject);
     mail.setHtmlBody(mbody);
    // file attachment
    // Reference the attachment page, pass in the account ID
    PageReference pdf =Page.GenererPdf ;
    pdf.getParameters().put('id',recordId );
    System.debug('pdf   >>>>'+  recordId );
    pdf.setRedirect(true);
    // Take the PDF content
    Blob b;
    if(Test.isRunningTest()) {
      b = blob.valueOf('Unit.Test');
    } else {
      b = pdf.getContent();
    }
    // Create the email attachment
    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
    efa.setFileName(titleFile+'.pdf');
    efa.setBody(b);
    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
   // Step 5. Add your email to the master list
     mails.add(mail);
 // Step 6: Send all emails in the master list
    Messaging.sendEmail(mails);
    System.debug('Email sending');
}
@AuraEnabled
public static Map<String,Object> getAttachment(Id recordId){
    Map<String,Object> response = new Map<String,Object>();
    try {
        response.put('error',false);
        Attachment attach = [SELECT Id, Name, ParentId, CreatedDate FROM Attachment Where ParentId = : recordId order by CreatedDate desc Limit 1];
        response.put('data',attach);
        system.debug('data from attachment'+ attach);
    } catch (Exception e) {
        System.debug('messages >>> '+e.getMessage());
        response.put('error',true);
        response.put('message',e.getMessage());
        response.put('cause',e.getCause());
    }
   return response;
}

@AuraEnabled
public static Quote getQuoteInfo(Id recordId){
    Quote quote;
    try {
        quote = [SELECT Account.Email__c, Owner.Name FROM Quote WHERE Id = :recordId WITH SECURITY_ENFORCED];
    } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
    return quote;
}
}