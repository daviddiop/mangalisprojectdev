@isTest
public with sharing class GenererContratPdfTest {
    @TestSetup
    static void setup(){
        Account acc = new Account(Name='test');
        insert acc;
        Hotel__c hotel = new Hotel__c(Name='Hotel Test', Adress__c='Test Address', Description__c='Test desc', Email__c='test@test.com', Matricule__c='N000', Pays__c='Senegal', Phone__c='777777', Ville__c='Thies');
        insert hotel;
        Segment__c segment = new Segment__c(Name='Segment Test');
        insert segment;
        Hotel_Type_Client__c htc = new Hotel_Type_Client__c(Name='HTC Test', Rate_Code__c='PCR1');
        insert htc;
        Pricebook2 pricebook = new Pricebook2(Name='Pricebook Test',Segment__c=segment.Id, Hotel_Type_Client__c= htc.Id);
        insert pricebook;
        List<Product2> produits = new List<Product2>();
        produits.add(new Product2(Name='test product', Hotel__c= hotel.Id));
        produits.add(new Product2(Name='productTest',CurrencyIsoCode='XOF', Hotel__c= hotel.Id));
        insert produits;
        List<PriceBookEntry> standPbks = new List<PriceBookEntry>();
        for( Product2 prod : produits){
            standPbks.add(new PriceBookEntry(
            Product2Id=prod.Id,
            Pricebook2Id=Test.getStandardPricebookId(),
            UnitPrice=2000,
            Tarif_Single__c=2000,
            IsActive=true
        	));
        }
        insert standPbks;
        List<PriceBookEntry> pbks = new List<PriceBookEntry>();
        for( Product2 prod : produits){
            pbks.add(new PriceBookEntry(
            Product2Id=prod.Id,
            Pricebook2Id=pricebook.Id,
            UnitPrice=2000,
            Tarif_Single__c=2000,
            IsActive=true
        	));
        }
        insert pbks;
        Opportunity opp=new Opportunity(
            Name='testquote',AccountId=acc.Id,
            Type_Compte__c='Direct',
            CloseDate=System.today(),
            StageName='Close Won',
            pricebook2Id = pricebook.Id
        );
        insert opp;
        Contract cont = new Contract(
            AccountId = acc.Id,
            Pricebook2Id=Test.getStandardPricebookId(),
            Status = 'Draft',
            StartDate = Date.today(),
            Opportunity__c = opp.Id,
            ContractTerm = 4
        );
        insert cont;
        Quote qe=new Quote(Name='quote test',OpportunityId=opp.Id);
        insert qe;
        List<QuoteLineItem> quoteLines = new List<QuoteLineItem>();
        for(PricebookEntry pbk : pbks) {
            quoteLines.add(new QuoteLineItem(
            QuoteId=qe.Id,
            Product2Id=pbk.Product2Id,
            UnitPrice=114000,
            PricebookEntryId=pbk.Id,
            Quantity=2
        ));
        }
        insert quoteLines;
    }

    @isTest
    static void accList(){
        Contract cont = [SELECT Id FROM Contract WHERE Account.Name='Test' LIMIT 1];
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(cont);
            PageReference pageRef = Page.CreerContrat;
            Test.setCurrentPage(pageRef);
            new GenererContratPdf(sc);
        Test.stopTest();
    }
}