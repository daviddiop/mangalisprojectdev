@isTest
public class TestPeriodeSeasonDateVerification {
    @testSetup static void setup(){
        Season__c season = new Season__c(Name='Season Test');
        insert season;
        Hotel__c hotel = new Hotel__c(Name='Hotel periode Test');
        insert hotel;
        period_season__c periode = new period_season__c(Season__c=season.Id, Hotel__c=hotel.Id, Start_Date__c=Date.newInstance(2023, 1, 1), End_Date__c=Date.newInstance(2023, 1, 31));
        insert periode;
    }
    @isTest static void test() {
        Season__c season = [SELECT Id, Name FROM Season__c WHERE Name='Season Test' LIMIT 1];
        Hotel__c hotel = [SELECT Id, Name FROM Hotel__c WHERE Name='Hotel periode Test' LIMIT 1];
        System.debug('### Season : '+ season);
        period_season__c newPeriode = new period_season__c(Season__c=season.Id, Hotel__c=hotel.Id, Start_Date__c=Date.newInstance(2023, 1, 10), End_Date__c=Date.newInstance(2023, 1, 20));
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(newPeriode, false);
        Test.stopTest();
        // Verify
        if (!result.isSuccess()) {
           System.assert(!result.isSuccess());
           System.assertNotEquals(null, result.getErrors().size());
        }
    }
}