public with sharing class calendarSeason {
    /**
* @description: To retrieve the most recent events
*/
    @AuraEnabled(cacheable=true)
    public static List<Event> fetchEvents() {
        List<Event> events = null;
        if(Event.SObjectType.getDescribe().isAccessible()){
            events = [SELECT Id, Subject, StartDateTime, IsAllDayEvent, EndDateTime 
                      FROM Event 
                      ORDER BY CreatedDate DESC
                      LIMIT 100];
        }
        return events;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<period_season__c> listPeriodeseason (Id recordId){
        List<period_season__c> listName = new List<period_season__c>();
        try {
            for (List<period_season__c> myList : [Select Season__r.Name,Hotel__r.Name,period_season__c.Start_Date__c,period_season__c.End_Date__c from period_season__c where Hotel__c =: recordId WITH SECURITY_ENFORCED ]) {
                listName.addAll(myList);
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return listName;
    }
}