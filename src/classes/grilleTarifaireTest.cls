@isTest
public class grilleTarifaireTest {
    
    @testSetup static void setup() {
        Hotel__c hotel = new Hotel__c(Name='Hotel Test pro', Adress__c='Test Address', Description__c='Test desc', Email__c='test@test.com', Matricule__c='N000', Pays__c='Senegal', Phone__c='777777', Ville__c='Thies');
        insert hotel;
        Segment__c segment = new Segment__c(Name='Segment Test pro');
        insert segment;
        Segment_Hotel__c segmentHotel = new Segment_Hotel__c(Name='Segment Hotel Test 1',Hotel__c=hotel.Id, Segment__c=segment.Id);
        insert segmentHotel;
        Hotel_Type_Client__c htc = new Hotel_Type_Client__c(Name='HTC Test 1', Segment_Hotel__c=segmentHotel.Id, Rate_Code__c='PCR1');
        insert htc;
        Id  standardPbId = Test.getStandardPricebookId();
        Pricebook2 pricebook = new Pricebook2(Name='Pricebook Test 1',Segment__c=segment.Id);
        insert pricebook;
        Product2 product = new Product2(Name='Product Test 1', Hotel__c=hotel.Id, Categorie_Chambre__c='Junior Suite');
        insert product;
        PricebookEntry standPriceBookEntry = new PricebookEntry(Product2Id=product.Id, Pricebook2Id=standardPbId, UnitPrice=10000);
        insert standPriceBookEntry;
        PricebookEntry priceBookEntry = new PricebookEntry(Product2Id=product.Id, Pricebook2Id=pricebook.Id, Hotel_Type_Client__c=htc.Id, UnitPrice=80000, Tarif_Single__c=80000, Tarif_Supplement_Double__c=14000);
        insert priceBookEntry;
    }
	@isTest static void getPriceBookEntriesTest(){
        Hotel__c hotel = [SELECT Id FROM Hotel__c WHERE Name='Hotel Test pro' LIMIT 1];
        System.debug('### Hotel : ' + hotel);
        Test.startTest();
        Map<String, List<PricebookEntry>> pricebookEntriesMap = grilleTarifiere.getPriceBookEntries(hotel.Id);
        Test.stopTest();
        System.debug('### Price Book Entries : ' + pricebookEntriesMap);
        System.assert(pricebookEntriesMap.size()!=0);
        System.assertEquals(1, pricebookEntriesMap.size());
    }
}