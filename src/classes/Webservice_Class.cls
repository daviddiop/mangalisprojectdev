@RestResource(UrlMapping ='/MangalisWebService/*')
global with sharing class Webservice_Class {
    @HttpGet
    global static List<sObject> getMethod() {
        RestRequest request = RestContext.request;
        // grab the ReservoirId from the end of the URL
        String param = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1
        );
        List<String> params = param.split('\\-');
        String fields = String.join(new List<String>(Schema.getGlobalDescribe().get(params[0]).getDescribe().fields.getMap().keySet()), ',');
        String query = 'SELECT '+fields+' FROM '+params[0];
        if(params.size() > 1)
            query += ' WHERE '+params[1].replace('+',' ');
        List<sObject> sobjList = Database.query(query);
        return sobjList;
    }
    @HttpPost
    global static List<sObject> insertMethod(List<sObject> sobjList){
        System.debug('@@@datas: '+sobjList);
        insert sobjList;
        return sobjList;
    }
}