public class ExportCalendarSeasonToCsvController {
    public String header{get;set;}
    public String hotelId{get;set;}
    public List<FinalMap> finalMapList{get; set;}
    public ApexPages.StandardController con {get; set;}
    public Map<String,List<wrapper>> periodeBySeason;
    public class wrapper{
        public String season{get; set;}        
        public String startDate{get; set;}
        public String endDate{get; set;}
    }
    public String Filetype{get;set;}
    public ExportCalendarSeasonToCsvController(ApexPages.StandardController controller){
        periodeBySeason = new Map<String,List<wrapper>>();
       hotelId = [SELECT Id FROM Hotel__c WHERE Id = :ApexPages.currentPage().getParameters().get('id')].Id;
        this.con=controller;  
        Filetype = '';
        finalMapList = new List<FinalMap>();
        header = 'Start Date,End Date\r\n';     
    }
    public void exportToExcel(){
        Integer currentYear=System.today().year();
        String year = currentYear.format();
        system.debug('currentYear :'+currentYear);
        String queryString = 'Select c.Season__r.Name,c.Start_Date__c,c.End_Date__c,c.Hotel__c,c.Current_Year__c from Calendar_season__c c where c.Hotel__c=\''+hotelId+'\'And c.current_year__c=\''+currentYear+'\''; 
        List<Calendar_season__c> lstCalendarseason = DataBase.Query(queryString);
        system.debug('lstCalendarseason :'+lstCalendarseason.size());
                system.debug('lstCalendarseason :'+lstCalendarseason);
        if(lstCalendarseason.size()>0){
            for(Calendar_season__c cs :lstCalendarseason){
                wrapper w = new wrapper();
                w.season = cs.Season__r.Name ;
                w.startDate = string.valueOf(cs.Start_Date__c);
                w.endDate = string.valueOf(cs.End_Date__c);
                 if(periodeBySeason.containsKey(w.season)){
                    periodeBySeason.get(w.season).add(w);
                }else{
                    periodeBySeason.put(w.season, new List<wrapper>{w});
                }
            }
            finalMapList = new List<FinalMap>();
            for(String season:periodeBySeason.keySet()){
                FinalMap fM= new FinalMap();
                fM.key=season;
                fM.values=periodeBySeason.get(season);
                finalMapList.add(fM);
            }
        }
        

    }
    class FinalMap{
        public String key{get; set;}
        public List<wrapper> values{get; set;}
    }
   
}