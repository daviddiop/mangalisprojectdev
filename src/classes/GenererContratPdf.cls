/**
 * @description       : 
 * @author            : Fama Ndiouck
 * @group             : 
 * @last modified on  : 04-26-2022
 * @last modified by  : Fama Ndiouck
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   03-14-2022   Fama Ndiouck   Initial Version
**/
public with sharing class GenererContratPdf {
    public final Contract contract;
    //public List<QuoteLineItem> quoteLines {get; set;}
    // public List<QuoteLineItem> quoteLine {get; set;}
    // public List<QuoteLineItem> quoteLin {get; set;}
    public String segmentName {get; set;}
    public String rateCode {get; set;}
    public String potentialNuity {get; set;}
    public List<Hotel__c> hotels {get; set;}
    public Map<String, List<QuoteLineItem>> quoteLineByHotel {get; set;}
    //public List<OpportunityLineItem> opportunityLines {get; set;}
    public GenererContratPdf(ApexPages.StandardController stdController) {
        this.contract = (Contract)stdController.getRecord();
       // this.quoteLines = [SELECT Id, Product2.Name,Product2.Categorie_Chambre__c,Product2.HotelName__c,UnitPrice,Quantity,Quote.Discount,TotalPrice,Quote.Created_Date__c FROM QuoteLineItem ];
       //this.opportunityLines = [SELECT Id, Product2.Name, UnitPrice FROM OpportunityLineItem];
        Contract cont = [SELECT Id, Opportunity__c FROM Contract WHERE Id=:this.contract.Id WITH SECURITY_ENFORCED];
     
       Opportunity opprecup = [Select id,Nuitee_Potentielle_Annuelle__c from Opportunity Where Id=:cont.Opportunity__c WITH SECURITY_ENFORCED ];
       this.potentialNuity = opprecup.Nuitee_Potentielle_Annuelle__c;
       Quote quote = [Select id,Opportunity.Pricebook2Id from Quote Where OpportunityId=:opprecup.Id WITH SECURITY_ENFORCED ];
        //this.quoteLine = [SELECT Id, Product2.Name,Product2.Categorie_Chambre__c,Product2.HotelName__c,UnitPrice,Quantity,Quote.Opportunity.Nuitee_Potentielle_Annuelle__c,Price_HS__c,Price_LS__c,Price_MS__c  FROM QuoteLineItem WHERE QuoteId=:quote.id AND Product2.HotelName__c LIKE 'Noom%'];
        //this.quoteLin = [SELECT Id, Product2.Name,Product2.Categorie_Chambre__c,Product2.HotelName__c,UnitPrice,Quantity, Quote.Opportunity.Nuitee_Potentielle_Annuelle__c,TotalPrice,Quote.Created_Date__c,Price_HS__c,Price_LS__c,Price_MS__c  FROM QuoteLineItem WHERE QuoteId=:quote.id AND Product2.HotelName__c LIKE 'Seen%' ];
        List<QuoteLineItem> quoteLines = [SELECT Id, Product2.Name,Product2.Categorie_Chambre__c,Product2.HotelName__c, Product2.Hotel__c, UnitPrice,Quantity, TotalPrice,Quote.Created_Date__c,Price_HS__c,Price_LS__c,Price_MS__c, Price_LMS__c, Whols__c  FROM QuoteLineItem WHERE QuoteId=:quote.id WITH SECURITY_ENFORCED ];
        this.quoteLineByHotel = new Map<String, List<QuoteLineItem>>();
        List<Id> hotelsId = new List<Id>();
        for(QuoteLineItem qli : quoteLines) {
            if (qli.Product2.HotelName__c== null) {continue;}
            if(this.quoteLineByHotel.containsKey(qli.Product2.HotelName__c)==false) {
                this.quoteLineByHotel.put(qli.Product2.HotelName__c, new List<QuoteLineItem>());
            }
            hotelsId.add(qli.Product2.Hotel__c);
            this.quoteLineByHotel.get(qli.Product2.HotelName__c).add(qli);
        }
        this.hotels = [ SELECT Name, Matricule__c, Description__c, Email__c, Adress__c, Pays__c, Phone__c, Ville__c FROM Hotel__c WHERE Id IN :hotelsId WITH SECURITY_ENFORCED];
        Pricebook2 pricebook = [ Select Segment__r.Name, Hotel_Type_Client__r.Rate_Code__c from Pricebook2 Where id=:quote.Opportunity.Pricebook2Id WITH SECURITY_ENFORCED];
        this.segmentName = pricebook.Segment__r.Name; 
        this.rateCode = pricebook.Hotel_Type_Client__r.Rate_Code__c; 
    }
}