/**
 * @description       : 
 * @author            : Fama Ndiouck
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : Fama Ndiouck
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   04-19-2022   Fama Ndiouck   Initial Version
**/
public with sharing class GenererQuotepdf {
    public final Quote quote;
    public String segmentName {get; set;}
    public String rateCode {get; set;}
    public List<QuoteLineItem> quoteLines {get; set;}

    public GenererQuotepdf (ApexPages.StandardController stdController) {
           this.quote = (Quote)stdController.getRecord();
            //this.quote = [SELECT QuoteNumber, Opportunity.Pricebook2Id, Created_Date__c, Quote.Contact.Name, Quote.Account.Name, Quote.Account.Type_Compte__c FROM Quote WHERE Id=:quote.Id WITH SECURITY_ENFORCED];
            this.quoteLines = [SELECT Id,Product2.Name,Product2.HotelName__c,UnitPrice,Quote.Account.Type_Compte__c,Price_HS__c,Price_LS__c,Price_MS__c, Price_LMS__c, Whols__c  FROM QuoteLineItem WHERE QuoteId=:quote.Id WITH SECURITY_ENFORCED];
            Quote quoterecup = [Select Opportunity.Pricebook2Id from Quote Where id=:this.quote.Id WITH SECURITY_ENFORCED ];
            Pricebook2 pricebook = [ Select Segment__r.Name, Hotel_Type_Client__r.Rate_Code__c from Pricebook2 Where id=:quoterecup.Opportunity.Pricebook2Id WITH SECURITY_ENFORCED];
            this.segmentName = pricebook.Segment__r.Name;
            this.rateCode = pricebook.Hotel_Type_Client__r.Rate_Code__c;
    }
}