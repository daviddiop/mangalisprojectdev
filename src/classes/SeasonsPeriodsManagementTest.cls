@isTest
public class SeasonsPeriodsManagementTest {
    @testSetup static void setup() {
        Hotel__c hotel = new Hotel__c(Name='Hotel Test');
        insert hotel;
    }
    
    @isTest static void createWeekendsDatasForHotelTest() {
        Hotel__c hotel = [SELECT Id FROM Hotel__c WHERE Name='Hotel Test'];
        Test.startTest();
        SeasonsPeriodsManagement.createWeekendsDatasForHotel(hotel.Id);
        Test.stopTest();
        Hotel__c resultHotel = [SELECT Id, (SELECT Id FROM period_seasons__r) FROM Hotel__c WHERE Id=:hotel.Id];
        System.debug('### Periodes season : ' + resultHotel.period_seasons__r);
        System.assert(resultHotel.period_seasons__r.size()>0);
        System.assertNotEquals(null, resultHotel.period_seasons__r.size());
    }
}