public with sharing class Connexion {
    public static string getToken() {
        
        HttpRequest tokenRequest = new HttpRequest();
        tokenRequest.setMethod('POST');
        tokenRequest.setEndpoint('https://test.api.amadeus.com/v1/security/oauth2/token');
        //String CLIENT_ID = 'Pkl0cATAPU162bANs67QHrWTwjM5mnZy';
        //String CLIENT_SECRET = 'FXX9jfyoZKc9icQp';
        
        String CLIENT_ID = 'WiQC6vkOzbgMnUUbACQf6m2vrbegpgJC';
        String CLIENT_SECRET = 'lZKBosZTgBMvzm9p';
        
        tokenRequest.setBody('grant_type=client_credentials' + '&client_id='+CLIENT_ID + 
                    '&client_secret='+CLIENT_SECRET);
        
        Http http = new Http();
        HTTPResponse responseToken = http.send(tokenRequest);
        Map<String, Object> authMap = (Map<String, Object>) JSON.deserializeUntyped(responseToken.getBody());
        
        system.debug(authMap);
        return (string)authMap.get('access_token');
        

    }
}