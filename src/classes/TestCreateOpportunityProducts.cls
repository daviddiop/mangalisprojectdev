@isTest
public class TestCreateOpportunityProducts {
    @testSetup static void setup()
        // Create Account test
        Account acc = new Account(Name = 'Test Account', Phone='+221777777777');
        insert acc;
        //Create Product test
        Product2 prd1 = new Product2 (Name='Test Product',Description='Test',productCode = 'ABC', isActive = true);
        insert prd1;
        // Create PriceBook
        PricebookEntry stanPbes = new PricebookEntry(Product2ID=prd1.Id,Pricebook2ID=Test.getStandardPricebookId(), UnitPrice=50000, isActive=true);
        insert stanPbes;
        
        Opportunity opp1 = new Opportunity (Name='Opp1 Test',StageName='Prospection',CloseDate=Date.today(),Pricebook2Id = Test.getStandardPricebookId(),Type_Compte__c='Direct', AccountId = acc.Id);
        insert opp1;
    }
    
    @isTest static void getPriceBookEntriesTest() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name='Opp1 Test'];
        System.debug('### Opportunity : ' + opp);
        Test.startTest();
        List<PricebookEntry> pricebookEntries = CreateOpportunityProducts.getPriceBookEntries(opp.Id);
        Test.stopTest();
        System.debug('### Price Book Entries : ' + pricebookEntries);
        System.assert(pricebookEntries.size()!=0);
        System.assertEquals(1, pricebookEntries.size());
    }
    
    @isTest static void createOpportunityLineItemsTest() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name='Opp1 Test'];
        Product2 product = [SELECT Id FROM Product2 WHERE Name='Test Product'];
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id=:product.Id AND Pricebook2Id=:Test.getStandardPricebookId()];
        
        List<OpportunityLineItem> oppData = new List<OpportunityLineItem>{new OpportunityLineItem(OpportunityID=opp.id,PriceBookEntryID=pbe.id, UnitPrice=1000)};
        
        Test.startTest();
        Boolean result = CreateOpportunityProducts.createOpportunityLineItems(JSON.serialize(oppData));
        Test.stopTest();
        System.assert(result);
        System.assertEquals(true, result);
    }
    
    @isTest static void createOpportunityLineItemsFalseTest() {
        List<OpportunityLineItem> oppData = new List<OpportunityLineItem>();
        Test.startTest();
        Boolean result = CreateOpportunityProducts.createOpportunityLineItems(JSON.serialize(oppData));
        Test.stopTest();
        System.assertEquals(false, result);
    }
}