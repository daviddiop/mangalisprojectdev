public with sharing class PriceListByProduct {
    @AuraEnabled(cacheable=true)
    public static Map<String, List<PricebookEntry>> getPriceBookEntries (Id recordId){
        Map<String, List<PricebookEntry>> pricebookEntryBySegment = new Map<String, List<PricebookEntry>>();
        try {
            List<PricebookEntry> namelist = [SELECT Pricebook2.Segment__r.Name, Hotel_Type_client__r.Rate_Code__c, Hotel_Type_Client__r.Nuity_Potential__c, Pricebook2.Name, Tarif_Single__c, Tarif_Supplement_double__c, 
            First_Offer_High_Season__c, First_Offer_Low_Season__c, First_Offer_Medium_Season__c, Second_Offer_High_Season__c, Second_Offer_Low_Season__c, Second_Offer_Medium_Season__c,
            Tariff_Max_High_Season__c, Tariff_Max_Low_Medium_Season__c, Tariff_Min_High_Season__c, Tariff_Min_Low_Medium_Season__c, Whols__c
            FROM PricebookEntry WHERE Product2Id =: recordId AND Hotel_Type_client__r.Rate_Code__c <>'WHOLS' WITH SECURITY_ENFORCED ];
            for( PricebookEntry pbk : namelist) {
                if (pbk.Pricebook2.Segment__r.Name== null) {continue;}
                if(pricebookEntryBySegment.containsKey(pbk.Pricebook2.Segment__r.Name)==false) {
                    pricebookEntryBySegment.put(pbk.Pricebook2.Segment__r.Name, new List<PricebookEntry>());
                }
                pricebookEntryBySegment.get(pbk.Pricebook2.Segment__r.Name).add(pbk);
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return pricebookEntryBySegment;
    }
}