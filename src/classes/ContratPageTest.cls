@isTest
public with sharing class ContratPageTest {
    @TestSetup
    static void setup(){
        Account testAccount = new Account(Name='test account', Email__c='test@test.com');
        insert testAccount;
        Contract cont = new Contract(
            AccountId = testAccount.Id,
            //Pricebook2Id=Test.getStandardPricebookId(),
            Status = 'Draft',
            StartDate = Date.today(),
            ContractTerm = 4
          
        );
        insert cont;
        }
    @isTest
	static void testSavePdf() {
        Contract contract = [SELECT Id FROM Contract WHERE Account.Name='test account'];
        Test.startTest();
        ContratPagePdf.savePdf(contract.Id);
        Test.stopTest();
    }
        
        @isTest
    static void testSavePdf2() {
        Test.startTest();
        ContratPagePdf.savePdf(null);
        Test.stopTest();
    }
    @isTest
    static void testSendMail() {
        Contract contract = [SELECT Id FROM Contract WHERE Account.Name='test account'];
        Test.startTest();
        ContratPagePdf.sendMailMethod('test@gmail.com','test','test',contract.Id,'Test');
        Test.stopTest();
    } 
    @isTest
    static void testGetAttachment() {
        Contract contract = [SELECT Id FROM Contract WHERE Account.Name='test account'];
        Test.startTest();
        ContratPagePdf.getAttachment(contract.Id);
        Test.stopTest();
    }
     
    
}