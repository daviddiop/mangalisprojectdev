public with sharing class Webservice_Opportunities {
    public Static void testOpportunities() {
        HttpRequest tokenRequest = new HttpRequest();
        tokenRequest.setMethod('POST');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://test.api.amadeus.com/v1/security/oauth2/token');
        request.setMethod('POST');
        // Set the body as a JSON object
        request.setBody('{"Opportunity Name":"Test1", "Account Name":"TestOpera", "Stage":"Fermée gagnante"}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if(response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
    }
        
    }